# Card Game

```plantuml
Game o-- Pile
class Game {
    int numberOfPlayers;
    public Player[] players;
    Pile pot;
    Pile draws;
    
    public Game(int numberOfCards, int numberOfPlayers)
    private void createPlayersWithCards(int numberOfPlayers, Pile deck)
    public void play()
}

Player o-- Pile


class Player {
    String name;
    Pile drawPile;
    Pile discardPile;
    
    Player(String name, Pile drawPile)
    public Card draw()
    public int cardsCount()
}

ArrayList *-- Card

class ArrayList< Card > {
  public boolean isEmpty()
  public int size()
  public boolean add(Card card)
  public boolean addAll(Collection<Card> card)  
}

enum Suit {
  CLUBS,
  SPADES,
  HEARTS,
  DIAMONDS
}
Card o-- Suit

class Pile extends ArrayList {
  public static Pile createDeck(int numberOfCards)
  public Pile getCardsForPlayer(int number)
  public void shuffle(Random random)
  public boolean uniqueMaximum(Card max, int maxIndex)
}

class Card {
    int number;
    Suit suit;
    
    public Card(int number, Suit suit)
}
```
