/**
 * The type Player.
 */
public class Player {
    /**
     * The Name.
     */
    final String name;
    /**
     * The Draw pile.
     */
    final Pile drawPile;
    /**
     * The Discard pile.
     */
    final Pile discardPile;

    /**
     * Instantiates a new Player.
     *
     * @param name     the name
     * @param drawPile the draw pile
     */
    public Player(String name, Pile drawPile) {
        this.name = name;
        this.drawPile = drawPile;
        this.discardPile = new Pile();
    }

    /**
     * Draw card.
     *
     * @return the card
     */
    public Card draw() {
        if (drawPile.isEmpty()) {
            discardPile.shuffle();
            drawPile.addAll(discardPile);
            discardPile.clear();
        }
        Card draw = drawPile.get(0);
        drawPile.remove(0);
        return draw;
    }

    /**
     * Cards count int.
     *
     * @return the int
     */
    public int cardsCount() {
        return drawPile.size() + discardPile.size();
    }
}
