import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    Game game;

    @org.junit.jupiter.api.Test

    @BeforeEach
    void setUp() {
        game = new Game(10, 2);
    }

    @Test
    void createDeck() {
        assertEquals(2, game.players.size());
        for (Player player : game.players) {
            assertEquals(40 / 2, player.drawPile.size());
        }
    }

    @Test
    void play() {
        game.players.play();
    }
}