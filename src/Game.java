public class Game {
    final Players players = new Players();
    private Pile deck;

    /**
     * @param numberOfCards   Total number of cards
     * @param numberOfPlayers Total number of players
     */
    public Game(int numberOfCards, int numberOfPlayers) {
        deck = Pile.createDeck(numberOfCards);
        int cardsPerPlayer = deck.size() / numberOfPlayers;
        for (int i = 0; i < numberOfPlayers; i++) {
            Player player = new Player(String.valueOf(i), deck.take(cardsPerPlayer));
            players.add(player);
        }
    }


}
