import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * The type Pile.
 */
public class Pile extends ArrayList<Card> {
    /**
     * Create deck pile.
     *
     * @param numberOfCards the number of cards
     * @return the pile
     */
    public static Pile createDeck(int numberOfCards) {
        Pile deck = new Pile();
        for (int number = 1; number <= numberOfCards; number++) {
            for (Card.Suit suit : Card.Suit.values()) {
                deck.add(new Card(number, suit));
            }
        }
        deck.shuffle();
        return deck;
    }

    /**
     * Take pile.
     *
     * @param number the number
     * @return the pile
     */
    public Pile take(int number) {
        Pile taken = new Pile();
        for (int i = 0; i < number; i++) {
            taken.add(get(0));
            remove(0);
        }
        return taken;
    }

    /**
     * Fisher-Yates shuffle()
     * https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
     *
     * @param random the random
     */
    public void shuffle(Random random) {
        for (int i = this.size() - 1; i > 0; i--) {
            Collections.swap(this, i, random.nextInt(i + 1));
        }
    }

    /**
     * Shuffle.
     */
    public void shuffle() {
        Random random = new Random();
        shuffle(random);
    }

    /**
     * Occurrences long.
     *
     * @param max the max
     * @return the long
     */
    public long occurrences(int max) {
        return IntStream.range(0, size()).filter(i -> get(i).number == max).count();
    }

    /**
     * Gets max.
     *
     * @return the max
     */
    public Card getMax() {
        Card max = get(0);
        for (int i = 1; i < this.size(); i++) {
            if (max.number < get(i).number) {
                max = get(i);
            }
        }
        return max;
    }
}
