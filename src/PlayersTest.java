import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayersTest {
    Players players = new Players();

    @BeforeEach
    void setUp() {
        Out.getOutput().clear();
    }

    @Test
    void playForZeroPlayers() {
        players.play();
        assertEquals("", Out.get());
    }

    @Test
    void playForOnePlayerMakesHimAWinner() {
        Player player = new Player("0", Pile.createDeck(1));
        players.add(player);
        players.play();
        assertEquals("Player 0 wins the game!\n", Out.get());
    }

    @Test
    void playWithSameCardsHasNoWinner() {
        Player player = new Player("0", Pile.createDeck(1));
        players.add(player);
        player = new Player("1", Pile.createDeck(1));
        players.add(player);
        players.play();
        assertTrue(Out.get().contains("No winner in this round"));
        assertTrue(Out.get().contains("No winner in the game!"));
    }

    @Test
    void playWithTwoPlayers() {
        Pile deck = new Pile();
        deck.add(new Card(1, Card.Suit.CLUBS));
        deck.add(new Card(2, Card.Suit.DIAMONDS));
        deck.add(new Card(3, Card.Suit.HEARTS));
        deck.add(new Card(4, Card.Suit.SPADES));
        Player player = new Player("0", deck.take(2));
        players.add(player);
        player = new Player("1", deck.take(2));
        players.add(player);
        players.play();
        assertTrue(Out.get().contains("Player 1 wins this round"));
        assertTrue(Out.get().contains("Player 1 wins the game!"));
    }

    @Test
    void playerOneWinningFourCards() {
        Pile deck = new Pile();
        deck.add(new Card(1, Card.Suit.CLUBS));
        deck.add(new Card(2, Card.Suit.DIAMONDS));
        deck.add(new Card(1, Card.Suit.HEARTS));
        deck.add(new Card(4, Card.Suit.SPADES));
        Player player = new Player("0", deck.take(2));
        players.add(player);
        player = new Player("1", deck.take(2));
        players.add(player);
        players.play();
        assertTrue(Out.get().contains("No winner in this round"));
        assertTrue(Out.get().contains("Player 1 wins this round"));
        assertTrue(Out.get().contains("Player 1 wins the game!"));
        assertEquals("1", players.get(0).name);
        assertEquals(4, players.get(0).cardsCount());
    }
}