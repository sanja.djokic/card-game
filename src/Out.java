import java.util.ArrayList;

/**
 * The type Out.
 */
public class Out {
    /**
     * The Output.
     */
    static final ArrayList<String> output = new ArrayList<>();

    /**
     * Instantiates a new Out.
     */
    protected Out() { throw new IllegalStateException("Utility class"); }

    /**
     * Gets output.
     *
     * @return the output
     */
    public static ArrayList<String> getOutput() {
        return output;
    }

    /**
     * Println.
     *
     * @param string the string
     */
    public static void println(String string) {
        output.add(string);
    }

    /**
     * Println.
     */
    public static void println() {
        println("");
    }

    /**
     * Get string.
     *
     * @return the string
     */
    public static String get() {
        StringBuilder string = new StringBuilder();
        for (String s : output) {
            string.append(s);
            string.append("\n");
        }
        return string.toString();
    }
}
