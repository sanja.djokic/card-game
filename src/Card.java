public class Card {
    final int number;
    final Suit suit;
    enum Suit {
        CLUBS,
        SPADES,
        HEARTS,
        DIAMONDS
    }

    public Card(int number, Suit suit) {
        this.number = number;
        this.suit = suit;
    }
}
