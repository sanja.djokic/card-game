import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class PileTest {
    Pile pile = new Pile();

    @Test
    void shuffleEmptyPile() {
        pile.shuffle();
        assertEquals(0, pile.size());
    }

    @Test
    void shuffleOneCard() {
        pile.add(new Card(1, Card.Suit.CLUBS));
        pile.shuffle();
        assertEquals(1, pile.size());
        assertEquals(1, pile.size());
    }

    @Test
    void shuffleCards() {
        pile.add(new Card(1, Card.Suit.CLUBS));
        pile.add(new Card(2, Card.Suit.DIAMONDS));
        pile.add(new Card(3, Card.Suit.HEARTS));
        pile.add(new Card(4, Card.Suit.SPADES));
        Random random = new Random(1);
        pile.shuffle(random);
        assertEquals(4, pile.get(0).number);
        assertEquals(Card.Suit.SPADES, pile.get(0).suit);
        assertEquals(1, pile.get(1).number);
        assertEquals(Card.Suit.CLUBS, pile.get(1).suit);
        assertEquals(2, pile.get(2).number);
        assertEquals(Card.Suit.DIAMONDS, pile.get(2).suit);
        assertEquals(3, pile.get(3).number);
        assertEquals(Card.Suit.HEARTS, pile.get(3).suit);
        assertEquals(4, pile.size());
    }

    @Test
    void createDeck() {
        pile = Pile.createDeck(1);
        assertEquals(1, pile.get(0).number);
        assertEquals(1, pile.get(1).number);
        assertEquals(1, pile.get(2).number);
        assertEquals(1, pile.get(3).number);
        assertEquals(4, pile.size());
    }

    @Test
    void createDeckWith40Cards() {
        pile = Pile.createDeck(10);
        assertEquals(40, pile.size());
    }

    @Test
    void take() {
        Pile taken;
        pile = Pile.createDeck(2);
        assertEquals(2 * 4, pile.size());
        taken = pile.take(0);
        assertEquals(0, taken.size());
        assertEquals(2 * 4, pile.size());
        taken = pile.take(3);
        assertEquals(3, taken.size());
        assertEquals(2 * 4 - 3, pile.size());
    }

    @Test
    void occurrences() {
        pile = Pile.createDeck(2);
        assertEquals(4, pile.occurrences(1));
        assertEquals(4, pile.occurrences(2));
        assertEquals(0, pile.occurrences(3));
    }

    @Test
    void getMax() {
        pile = Pile.createDeck(2);
        assertEquals(2, pile.getMax().number);
    }
}