import java.util.ArrayList;

/**
 * The type Players.
 */
public class Players extends ArrayList<Player> {
    private final Pile pot = new Pile();
    private final Pile draws = new Pile();

    /**
     * Play.
     */
    public void play() {
        if (size() > 0) {
            while (playersLeft() > 1) {
                draws.clear();
                for (Player player : this) {
                    String output = String.format("Player %s (%d cards)", player.name, player.cardsCount());
                    Card card = player.draw();
                    draws.add(card);
                    Out.println(String.format("%s: %d - %s", output, card.number, card.suit));
                }
                pot.addAll(draws);
                givePotOnWin();
            }
            if (size() == 0) {
                Out.println("No winner in the game!");
            } else {
                Out.println(String.format("Player %s wins the game!", get(0).name));
            }
        }
    }

    private int playersLeft() {
        for (int i = size() - 1; i >= 0; i--) {
            if (get(i).cardsCount() == 0) {
                remove(i);
            }
        }
        return size();
    }

    private void givePotOnWin() {
        Card max = draws.getMax();
        if (draws.occurrences(max.number) == 1) {
            int maxIndex = draws.indexOf(max);
            Out.println(String.format("Player %d wins this round", maxIndex));
            this.get(maxIndex).discardPile.addAll(pot);
            pot.clear();
        } else {
            Out.println("No winner in this round");
        }
        Out.println();
    }
}
