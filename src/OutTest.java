import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OutTest {

    @Test
    void printlnStoresString() {
        Out.getOutput().clear();
        Out.println();
        assertEquals("\n", Out.get());
        Out.println("Hello");
        assertEquals("\nHello\n", Out.get());
    }

    @Test
    void throwsException() {
        assertThrows(IllegalStateException.class, Out::new);
    }
}