import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    Player player;
    Pile pile;

    @BeforeEach
    void setUp() {
        pile = Pile.createDeck(1);
        player = new Player("0", pile);
    }

    @Test
    void drawWithEmptyDrawPile() {
        int n = player.drawPile.size();
        player.discardPile.addAll(player.drawPile.take(n));
        assertEquals(0, player.drawPile.size());
        assertEquals(n, player.discardPile.size());
        player.draw();
        assertEquals(n - 1, player.drawPile.size());
        assertEquals(0, player.discardPile.size());
    }

    @Test
    void draw() {
        Card card = player.draw();
        assertEquals(1, card.number);
    }

    @Test
    void cardsCount() {
        assertEquals(4, player.cardsCount());
    }
}