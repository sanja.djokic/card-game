import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test

    void constructorWorks() {
        Card card = new Card(1, Card.Suit.CLUBS);
        assertEquals(1, card.number);
        assertEquals(Card.Suit.CLUBS, card.suit);
    }
}