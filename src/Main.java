/**
 * Card Game
 */
public class Main {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Game game = new Game(10, 2);
        game.players.play();
        System.out.print(Out.get());
    }
}
